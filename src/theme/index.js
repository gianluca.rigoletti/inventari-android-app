// Theme variables
export const primaryColor = "#f57c00"
export const primaryColorLight = "#ffad42"
export const primaryColorDark = "#bb4d00"

export const secondaryColor = "#1565c0"
export const secondaryColorLight = "#5e92f3"
export const secondaryColorDark = "#003c8f"
export const successColor = "#4CAF50"
export const alertColor = "#d50000"

export const whiteTextColor = "#ffffff"
export const primaryTextColor = "rgba(0, 0, 0, 0.87)"
export const secondaryTextColor = "rgba(0, 0, 0, 0.54)"
export const disabledTextColor = "rgba(0, 0, 0, 0.38)"
export const dividersTextColor = "rgba(0, 0, 0, 0.12)"
export const transparent = "rgba(0,0,0,0.1)"

export const activeIconColor = secondaryTextColor
export const inactiveIconColor = disabledTextColor

export const grey1 = "#e0e0e0"
export const grey2 = "#f5f5f5"
export const grey3 = "#fafafa"
export const white = "#ffffff"

export const fontSizeIcon = 24
export const fontSize1 = 20
export const fontSize2 = 16
export const fontSize3 = 14
export const fontSize4 = 12
export const fontSize5 = 10
export const displayFontSize = 32

export const spacing1 = 2
export const spacing2 = 4
export const spacing3 = 8
export const spacing4 = 16
export const spacing5 = 32
