import RNFetchBlob from "react-native-fetch-blob"
import configs from "configs"

class Log {
  constructor(writePath) {
    this.writePath = writePath
    this.initializeFolder()
  }

  get day() {
    return new Date()
      .toISOString()
      .slice(0, 10)
      .replace(/-/g, "")
  }

  get fileName() {
    return this.day + ".txt"
  }

  get completeFilePath() {
    return this.writePath + this.fileName
  }

  log(message) {
    this.writeFile("log", message)
  }

  warn(message) {
    this.writeFile("warn", message)
  }

  error(message) {
    this.writeFile("error", message)
  }

  writeFile(type, message) {
    RNFetchBlob.fs
      .appendFile(
        this.completeFilePath,
        `${new Date().toISOString()} [${type.toUpperCase()}] ${message}\n`
      )
      .catch(err => console.log(err))
  }

  initializeFolder() {
    console.log(this.writePath)
    RNFetchBlob.fs.mkdir(this.writePath).catch(() => {})
  }
}

export default new Log(configs.logPath)
