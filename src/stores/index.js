import NavigatorsStore from "stores/NavigatorsStore"
import ViewsStore from "stores/ViewsStore"
import FSStore from "stores/FSStore"

export default {
  navigators: new NavigatorsStore(),
  views: new ViewsStore(),
  fs: new FSStore()
}
