import { observable, action } from "mobx"
import { NavigationActions } from "react-navigation"
import RootNavigator from "navigators/RootNavigator"
import stores from "stores"

export default class NavigatorsStore {
  @observable.ref
  navigationState = RootNavigator.router.getStateForAction(
    NavigationActions.navigate({
      routeName: "HomeDrawer"
    })
  )

  @action
  dispatch = (action, stackNavState = true) => {
    let dispatchedAction = action
    if (
      action.type === "Navigation/NAVIGATE" &&
      action.routeName === "SceltaDrawer"
    ) {
      stores.views.setScanType("scaffale")
    }
    if (
      action.type === "Navigation/NAVIGATE" &&
      action.routeName === "HomeDrawer"
    ) {
      dispatchedAction = NavigationActions.reset({
        index: 0,
        key: null,
        actions: [
          NavigationActions.navigate({
            routeName: "Root"
          })
        ]
      })
    }
    const previousNavState = stackNavState ? this.navigationState : null
    return (this.navigationState = RootNavigator.router.getStateForAction(
      dispatchedAction,
      previousNavState
    ))
  }
}
