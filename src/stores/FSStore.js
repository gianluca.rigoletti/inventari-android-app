import { observable, computed, action } from 'mobx'

export default class FSStore {
    @observable filePath
    @observable exportProgress
    @observable uploadProgress
    @observable importProgress

    constructor() {
        this.filePath = ''
        this.exportProgress = 0 
    }

    @computed get isDownloadComplete() {
        return this.filePath !== ''
    }
    
    @computed get isExportCompleted() {
        return this.exportProgress === 1
    }

    @action setExportProgress(value) {
        this.exportProgress = value
    }

    @action setImportProgress(value) {
        this.importProgress = value
    }

}