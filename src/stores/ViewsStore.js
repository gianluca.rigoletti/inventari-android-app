import { observable, action, computed } from "mobx"
import DB from "db"
import _ from "lodash"
import Log from "log"

export default class ViewsStore {
  @observable tipoConta
  @observable codiceScaffale
  @observable codiceArticolo
  @observable quantitaArticolo
  @observable scanType
  @observable generalArticoloBarcode
  @observable generalScaffaleBarcode
  @observable beep

  constructor() {
    this.tipoConta = null
    this.codiceScaffale = ""
    this.codiceArticolo = ""
    this.quantitaArticolo = ""
    this.scanType = "Scaffale"
    this.generalArticoloBarcode = ""
    this.generalScaffaleBarcode = ""
    this.beep = null
  }

  @action
  setGeneralArticoloBarcode(barcode) {
    this.generalArticoloBarcode = barcode
  }

  @action
  setGeneralScaffaleBarcode(barcode) {
    this.generalScaffaleBarcode = barcode
  }

  @action
  setTipoConta(tipoConta) {
    this.tipoConta = tipoConta
  }

  @action
  setCodiceScaffale(codiceScaffale) {
    this.codiceScaffale = codiceScaffale
  }

  @action
  setCodiceArticolo(codiceArticolo) {
    this.codiceArticolo = codiceArticolo
  }

  @action
  setQuantitaArticolo(quantitaArticolo) {
    this.quantitaArticolo = quantitaArticolo
  }

  @action
  setScanType(scanType) {
    this.scanType = scanType
  }

  @action
  setBeep(beep) {
    this.beep = beep
  }

  @computed
  get corsia() {
    return this.codiceScaffale.slice(0, 3)
  }

  @computed
  get scaffale() {
    return this.codiceScaffale.slice(3, 6)
  }

  @computed
  get descrizioneArticolo() {
    if (this.codiceArticolo !== "") {
      return DB.getDescrizioneArticolo(this.codiceArticolo)
    }

    return null
  }

  @computed
  get alreadyReadQuantity() {
    if (this.codiceArticolo !== "") {
      const articoli = !this.isSpecialSettore
        ? DB.getExistingArticoli(
            this.codiceArticolo,
            true,
            this.corsia,
            this.scaffale
          )
        : DB.getExistingArticoli(this.codiceArticolo)
      Log.log(`Numero articoli in getAlreadyReadQuantity: ${articoli.length}`)
      if (articoli.length) {
        return _.sumBy(articoli, "quantita")
      }
    }
    return 0
  }

  @computed
  get isSpecialSettore() {
    return this.codiceScaffale === "999999"
  }
}
