import { StackNavigator } from "react-navigation"
import ImportaScreen from "screens/ImportaScreen"
import EsportaScreen from "screens/EsportaScreen"
import SceltaNavigator from "navigators/SceltaNavigator"
import HomeScreen from "screens/HomeScreen"
import { Easing, Animated } from "react-native"

export default StackNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    Importa: {
      screen: ImportaScreen
    },
    Esporta: {
      screen: EsportaScreen
    },
    SceltaStack: {
      screen: SceltaNavigator
    }
  },
  {
    transitionConfig: () => ({
      transitionSpec: {
        duration: 350,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing
      }
    })
  }
)
