import React from "react"
import { DrawerNavigator } from "react-navigation"
import HomeNavigator from "navigators/HomeNavigator"
import SceltaNavigator from "navigators/SceltaNavigator"
import CustomDrawer from "views/customdrawer"

export default DrawerNavigator(
  {
    HomeDrawer: {
      screen: HomeNavigator
    },
    SceltaDrawer: {
      screen: SceltaNavigator
    }
  },
  {
    contentComponent: props => <CustomDrawer {...props} />
  }
)
