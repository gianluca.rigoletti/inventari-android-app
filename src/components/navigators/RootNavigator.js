import { StackNavigator } from "react-navigation"
import DrawerNavigator from "navigators/DrawerNavigator"

export default StackNavigator(
  {
    Root: {
      screen: DrawerNavigator
    }
  },
  {
    headerMode: "none"
  }
)
