import { StackNavigator } from "react-navigation"
import SceltaScreen from "screens/SceltaScreen"
import ScanScreen from "screens/ScanScreen"

export default StackNavigator({
  Scelta: {
    screen: SceltaScreen
  },
  Scan: {
    screen: ScanScreen
  }
})
