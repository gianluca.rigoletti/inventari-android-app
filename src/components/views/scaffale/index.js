import React from "react"
/*eslint-disable no-unused-vars*/
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert
} from "react-native"
import { observer, inject } from "mobx-react"
import { reaction } from "mobx"
import Camera from "react-native-camera"
import _ from "lodash"
import DB from "db"
import * as theme from "theme"

@inject("views")
@observer
export default class Scaffale extends React.Component {
  constructor(props) {
    super(props)
  }

  barcodeReaction = reaction(
    () => this.props.views.generalScaffaleBarcode,
    generalScaffaleBarcode => this.setBarcodeData(generalScaffaleBarcode)
  )

  componentWillMount() {
    this.setBarcodeData = _.throttle(this.setBarcodeData, 2000)
  }

  setBarcodeData = data => {
    this.props.views.beep.play(() => {})
    const codiceScaffale = this.stripNonDigitsChars(data.data)
    this.props.views.setCodiceScaffale(codiceScaffale)
  }

  checkExistingCodice() {}

  validateCodiceScaffale = () => {
    const barcode = this.props.views.codiceScaffale
    // check for length
    const barcodeLength = barcode.length
    // check of just digits
    const isNumber = /^\d+$/.test(barcode)
    return barcode !== undefined && barcodeLength === 6 && isNumber
  }

  scanArticles = () => {
    // TODO: check for already opened scaffale
    const isBarcodeValid = this.validateCodiceScaffale()
    if (!isBarcodeValid) {
      Alert.alert("Il codice scaffale inserito non è valido")
      this.props.views.codiceScaffale = ""
    } else {
      // checkOpenedScaffale()
      const corsia = parseInt(this.props.views.corsia)
      const scaffale = parseInt(this.props.views.scaffale)
      if (
        DB.isMissingOrInvalidSettore(corsia, scaffale) &&
        !this.props.views.isSpecialSettore
      ) {
        Alert.alert(
          "Attenzione",
          "Settore invalido o mancante. Inserisci di nuovo il codice",
          [{ text: "Ok", onPress: () => {} }],
          { cancelable: false }
        )
      } else if (!DB.isSettoreGiaLetto(corsia, scaffale)) {
        // Insert scaffale into db
        DB.insertSettore({
          corsia,
          scaffale
        })
        this.props.views.setScanType("singola/multipla")
      } else {
        Alert.alert(
          "Attenzione",
          "Settore già letto",
          [
            { text: "Annulla", onPress: () => {} },
            {
              text: "Prosegui",
              onPress: () => this.props.views.setScanType("singola/multipla")
            }
          ],
          { cancelable: false }
        )
      }
    }
  }

  stripNonDigitsChars = input => {
    return input.replace(/\D/g, "")
  }

  checkMaxLength = (input, maxLength = 6) => {
    return input.substr(0, maxLength)
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          selectTextOnFocus={true}
          style={styles.textInput}
          placeholder="Scaffale"
          onChangeText={text =>
            this.props.views.setCodiceScaffale(
              this.stripNonDigitsChars(this.checkMaxLength(text))
            )}
          value={this.props.views.codiceScaffale}
          keyboardType="numeric"
          onBlur={() => this.scanArticles()}
          placeholderTextColor={theme.disabledTextColor}
          selectionColor={theme.secondaryColor}
          underlineColorAndroid={theme.secondaryColor}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.scanArticles()}
        >
          <Text style={styles.buttonText}>AVANTI</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.grey3
  },
  textInput: {
    color: theme.primaryTextColor,
    marginLeft: theme.spacing3,
    marginRight: theme.spacing3,
    fontSize: theme.fontSize2
  },
  button: {
    backgroundColor: theme.secondaryColor,
    padding: theme.spacing4,
    margin: theme.spacing3
  },
  buttonText: {
    color: theme.whiteTextColor,
    alignSelf: "center"
  }
})

AppRegistry.registerComponent("Inventari", () => Scaffale)
