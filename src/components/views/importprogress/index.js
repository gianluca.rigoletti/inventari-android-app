import React from 'react';
import { AppRegistry, StyleSheet, Text, View, Button, ProgressBarAndroid } from 'react-native';

export default class ImportProgress extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.isActive) {
            return (
                <View>
                    <Text>Importando il file nel database</Text>
                    <ProgressBarAndroid 
                        color="deepskyblue"
                        styleAttr="Horizontal"
                    />
                </View>
            )
        }
        if (this.props.isCompleted) {
            return (
                <View>
                    <Text style={style.completed}>Importazione completata</Text>
                </View>
            )
        }
        return (
            <View>
                <Text>Importare il file</Text>
            </View>
        )
    }
}


const style = StyleSheet.create({
    active: {
        color: 'deepskyblue',
    },
    completed: {
        color: 'grey',
    }
})

AppRegistry.registerComponent('Inventari', () => ImportProgress)
