import React from "react"
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ProgressBarAndroid
} from "react-native"
import * as theme from "theme"
import Icon from "react-native-vector-icons/MaterialIcons"

export default class DownloadProgress extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={ss.container}>
        {this.props.download ? (
          <Text style={ss.title}>{this.props.type}</Text>
        ) : null}
        {this.props.active ? (
          <View style={ss.activeContainer}>
            {this.props.download ? (
              <Text style={ss.activeText}>Download in corso</Text>
            ) : (
              <Text style={ss.activeText}>Importazione in corso</Text>
            )}
            <ProgressBarAndroid
              color={theme.secondaryColor}
              styleAttr="Horizontal"
              indeterminate={true}
            />
          </View>
        ) : this.props.completed ? (
          <View style={ss.completedContainer}>
            {this.props.download ? (
              <Text style={ss.completedText}>Download completato</Text>
            ) : (
              <Text style={ss.completedText}>Importazione completata</Text>
            )}
            <Icon name="done-all" style={ss.completedIcon} />
          </View>
        ) : this.props.alreadyDownloaded ? (
          <View style={ss.alreadyDownloadedContainer}>
            <Text style={ss.alreadyDownloadedText}>
              {this.props.type} già scaricati
            </Text>
            <Icon name="done" style={ss.completedIcon} />
          </View>
        ) : (
          <View style={ss.disabledContainer}>
            {this.props.download ? (
              <Text style={ss.disabledText}>Pronto per il download</Text>
            ) : (
              <Text style={ss.disabledText}>Pronto l'importazione </Text>
            )}
          </View>
        )}
      </View>
    )
  }
}

const ss = StyleSheet.create({
  container: {
    backgroundColor: theme.white,
    paddingLeft: theme.spacing4,
    paddingRight: theme.spacing4
  },
  title: {
    fontWeight: "800",
    fontSize: theme.fontSize2,
    color: theme.primaryTextColor,
    paddingTop: theme.spacing4,
    paddingBottom: theme.spacing4
  },
  disabledContainer: {
    paddingTop: theme.spacing2,
    paddingBottom: theme.spacing4
  },
  disabledText: {
    color: theme.disabledTextColor
  },
  activeContainer: {
    paddingRight: theme.spacing4
  },
  activeText: {
    color: theme.primaryTextColor,
    fontSize: theme.fontSize3
  },
  completedContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: theme.spacing2,
    paddingBottom: theme.spacing4
  },
  completedText: {
    color: theme.primaryTextColor
  },
  completedIcon: {
    paddingLeft: theme.spacing4,
    fontSize: theme.fontSize2,
    color: theme.successColor
  },
  alreadyDownloadedContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: theme.spacing2,
    paddingBottom: theme.spacing4
  },
  alreadyDownloadedText: {
    color: theme.primaryTextColor
  }
})

AppRegistry.registerComponent("Inventari", () => DownloadProgress)
