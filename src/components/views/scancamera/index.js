import React from "react"
import { StyleSheet, View, AppRegistry, Alert } from "react-native"
import { observer, inject } from "mobx-react"
import BarcodeScanner, {
  Exception,
  FocusMode,
  BarcodeType
} from "react-native-barcode-scanner-google"
import * as theme from "theme"
import _ from "lodash"
import DB from "db"

@inject("views")
@observer
export default class ScanCamera extends React.Component {
  constructor(props) {
    super(props)
  }

  validateCodiceScaffale = () => {
    const barcode = this.props.views.codiceScaffale
    // check for length
    const barcodeLength = barcode.length
    // check of just digits
    const isNumber = /^\d+$/.test(barcode)
    return barcode !== undefined && barcodeLength === 6 && isNumber
  }

  scanArticles = () => {
    // TODO: check for already opened scaffale
    const isBarcodeValid = this.validateCodiceScaffale()
    if (!isBarcodeValid) {
      Alert.alert("Il codice scaffale inserito non è valido")
      this.props.views.codiceScaffale = ""
    } else {
      // checkOpenedScaffale()
      const corsia = parseInt(this.props.views.corsia)
      const scaffale = parseInt(this.props.views.scaffale)
      if (
        DB.isMissingOrInvalidSettore(corsia, scaffale) &&
        !this.props.views.isSpecialSettore
      ) {
        Alert.alert(
          "Attenzione",
          "Settore invalido o mancante. Inserisci di nuovo il codice",
          [{ text: "Ok", onPress: () => {} }],
          { cancelable: false }
        )
      } else if (!DB.isSettoreGiaLetto(corsia, scaffale)) {
        // Insert scaffale into db
        DB.insertSettore({
          corsia,
          scaffale
        })
        this.props.views.setScanType("singola/multipla")
      } else {
        Alert.alert(
          "Attenzione",
          "Settore già letto",
          [
            { text: "Annulla", onPress: () => {} },
            {
              text: "Prosegui",
              onPress: () => this.props.views.setScanType("singola/multipla")
            }
          ],
          { cancelable: false }
        )
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.props.children}
        <View style={styles.cameraWrapper}>
          <BarcodeScanner
            style={styles.barcodeCamera}
            onBarCodeRead={_.throttle(
              data => {
                if (this.props.views.scanType === "singola/multipla") {
                  this.props.views.setGeneralArticoloBarcode(data)
                } else {
                  this.props.views.setGeneralScaffaleBarcode(data)
                  this.scanArticles()
                }
              },
              1000,
              {
                leading: true,
                trailing: false
              }
            )}
            onException={exceptionKey => {
              // check instructions on Github for a more detailed overview of these exceptions.
              switch (exceptionKey) {
                case Exception.NO_PLAY_SERVICES:
                // tell the user they need to update Google Play Services
                case Exception.LOW_STORAGE:
                // tell the user their device doesn't have enough storage to fit the barcode scanning magic
                case Exception.NOT_OPERATIONAL:
                // Google's barcode magic is being downloaded, but is not yet operational.
                default:
                  break
              }
            }}
            focusMode={FocusMode.AUTO /* could also be TAP or FIXED */}
            barcodeType={
              BarcodeType.CODE_128 |
              BarcodeType.EAN_13 |
              BarcodeType.EAN_8 /* replace with ALL for all alternatives */
            }
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: theme.white
  },
  cameraWrapper: {
    flex: 1
  },
  barcodeCamera: {
    height: "100%"
  }
})

AppRegistry.registerComponent("Inventari", () => ScanCamera)
