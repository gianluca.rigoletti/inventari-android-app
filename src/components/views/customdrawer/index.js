import React from "react"
import { StyleSheet, View, Text } from "react-native"
import { DrawerItems } from "react-navigation"
import * as theme from "theme"

export default props => (
  <View style={ss.container}>
    <View style={ss.splash}>
      <Text style={ss.title}>Inventari app</Text>
    </View>
    <DrawerItems
      activeTintColor={theme.primaryColor}
      inactiveTintColor={theme.primaryTextColor}
      inactiveBackgroundColor={theme.white}
      style={ss.item}
      labelStyle={ss.label}
      {...props}
    />
    <View style={ss.versionContainer}>
      <Text style={ss.versionText}>Versione 0.1</Text>
    </View>
  </View>
)

const ss = StyleSheet.create({
  label: {
    width: "100%"
  },
  container: {
    flexDirection: "column"
  },
  splash: {
    height: theme.spacing5 * 4,
    alignItems: "flex-start",
    justifyContent: "flex-end",
    backgroundColor: theme.primaryColor
  },
  title: {
    color: theme.white,
    fontSize: theme.displayFontSize,
    padding: theme.spacing4
  },
  versionContainer: {
    margin: theme.spacing3,
    height: 360,
    flexDirection: "column",
    justifyContent: "flex-end"
  },
  versionText: {
    fontSize: theme.fontSize2,
    padding: theme.spacing4,
    fontWeight: "200"
  }
})
