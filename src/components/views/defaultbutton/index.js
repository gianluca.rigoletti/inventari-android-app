import React from "react"
import { AppRegistry, StyleSheet, Text, TouchableOpacity } from "react-native"
import * as theme from "theme"

export default class DefaultButton extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const buttonColorStyle = this.props.alert
      ? ss.alertButton
      : ss.defaultButton
    return (
      <TouchableOpacity
        disabled={this.props.disabled}
        style={[
          ss.button,
          buttonColorStyle,
          this.props.disabled && ss.buttonDisabled
        ]}
        onPress={() => this.props.onPress()}
      >
        <Text style={ss.buttonText}>{this.props.title}</Text>
      </TouchableOpacity>
    )
  }
}

const ss = StyleSheet.create({
  buttonText: {
    fontWeight: "800",
    color: theme.whiteTextColor,
    fontSize: theme.fontSize3,
    textAlign: "center"
  },
  buttonDisabled: {
    backgroundColor: theme.secondaryColorLight
  },
  button: {
    padding: theme.spacing4,
    margin: theme.spacing3
  },
  defaultButton: {
    backgroundColor: theme.secondaryColor
  },
  alertButton: {
    backgroundColor: theme.alertColor
  }
})

AppRegistry.registerComponent("Inventari", () => DefaultButton)
