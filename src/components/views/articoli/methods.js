import DB from "db"
import RNFetchBlob from "react-native-fetch-blob"
import Log from "log"
import configs from "configs"
import { Alert } from "react-native"

export const validateId = idArticolo => {
  if (this.props.views.isSpecialSettore) {
    console.log(`Settore speciale`)
    const isNumber = /^\d ()+-$/.test(idArticolo)
  }
  const isNumber = /^\d$/.test(idArticolo)
  return isNumber
}

export const stripNonDigitsChars = (input, isSpecialSettore) => {
  if (this.props.views.isSpecialSettore) {
    return input.replace(/\D ()+-/g, "")
  }
  return input.replace(/\D/g, "")
}

export const handleBarcodeFromCamera = data => {
  this.props.views.setCodiceArticolo(parseInt(data.data))
  this.handleArticleCode()
}

export const handleArticleCode = () => {
  const codiceArticolo = this.props.views.codiceArticolo
  this.props.views.codiceArticolo = this.stripNonDigitsChars(codiceArticolo)
  const articolo = DB.getExistingArticolo(codiceArticolo)
  this.alreadyReadQuantity = articolo ? articolo.quantita : 0
  if (this.props.views.tipoConta === "singola") {
    this.handleQuantity("1")
  } else {
    this.QuantitaInput.focus()
  }
}

export const handleQuantity = quantity => {
  if (quantity > 1000) {
    Alert.alert("La quantità inserita è troppo alta")
  } else {
    this.props.views.setQuantitaArticolo(this.stripNonDigitsChars(quantity))
    this.isQuantityValid = true
  }
}

export async function submitArticle() {
  const articolo = {
    codice: parseInt(this.props.views.codiceArticolo),
    scaffale: parseInt(this.props.views.scaffale),
    quantita:
      parseInt(this.props.views.alreadyReadQuantity) +
      parseInt(this.props.views.quantitaArticolo),
    settore: parseInt(this.props.views.corsia)
  }
  console.log(`Inserimento articolo: <Articoli> ${JSON.stringify(articolo)}`)
  // TODO: implement something like DB.insertArticle(articolo)
  await DB.insertArticolo(articolo)
  ToastAndroid.showWithGravity(
    "Articolo inserito",
    ToastAndroid.SHORT,
    ToastAndroid.CENTER
  )
  this.setNewArticle()
}

export const setNewArticle = () => {
  this.props.views.setCodiceArticolo("")
  this.alreadyReadQuantity = 0
  if (this.props.views.tipoConta === "singola") {
    this.props.views.setQuantitaArticolo("1")
  } else {
    this.props.views.setQuantitaArticolo("")
  }
}

export const setNewScaffale = () => {
  this.props.views.setCodiceScaffale("")
  this.props.views.setScanType("scaffale")
}
