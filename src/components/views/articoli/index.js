import React from "react"
import {
  StyleSheet,
  View,
  AppRegistry,
  Text,
  TextInput,
  TouchableOpacity,
  Alert
} from "react-native"
import Snackbar from "react-native-snackbar"
import { observer, inject } from "mobx-react"
import _ from "lodash"
import { observable, reaction } from "mobx"
import DB from "db"
import * as theme from "theme"
import Log from "log"

@inject("views")
@observer
export default class Articoli extends React.Component {
  @observable isIdValid
  @observable isQuantityValid
  @observable activeCamera
  constructor(props) {
    super(props)
    this.QuantitaInput
    this.isIdValid = false
    this.isQuantityValid = false
    this.activeCamera = true
    this.ArticleCode
  }

  barcodeReaction = reaction(
    () => this.props.views.generalArticoloBarcode,
    generalArticoloBarcode =>
      this.handleBarcodeFromCamera(generalArticoloBarcode)
  )

  componentWillMount() {
    this.handleBarcodeFromCamera = _.throttle(this.handleBarcodeFromCamera, 500)
  }

  stripNonDigitsChars = input => {
    if (this.props.views.isSpecialSettore) {
      return input.replace(/[^\d.+-]/g, "")
    }
    return input.replace(/[^\d.]/g, "")
  }

  enableCamera = () => {
    this.activeCamera = true
  }

  disableCamera = () => {
    this.activeCamera = false
  }

  handleBarcodeFromCamera = data => {
    console.debug(`Camera is ${this.activeCamera}`)
    if (!this.activeCamera) return
    this.props.views.beep.play(() => {})
    this.props.views.setCodiceArticolo(data.data)
    this.handleArticleCode()
  }

  handleArticleCode = () => {
    const codiceArticolo = this.props.views.codiceArticolo
    this.props.views.codiceArticolo = this.stripNonDigitsChars(codiceArticolo)
    console.log(this.props.views.codiceArticolo)
    if (!this.isEmptyDescrizioneArticolo()) {
      if (this.props.views.tipoConta === "singola") {
        this.handleQuantity("1")
      } else {
        if (this.QuantitaInput !== null) {
          this.QuantitaInput.focus()
        }
      }
    }
  }

  handleQuantity = quantity => {
    const quantityTooHigh = quantity > 1000
    const negativeQuantityTooHigh = this.props.views.isSpecialSettore
      ? Math.abs(quantity) > this.props.views.alreadyReadQuantity
      : false
    if (quantityTooHigh || negativeQuantityTooHigh) {
      Alert.alert("La quantità inserita è troppo alta")
    } else {
      this.props.views.setQuantitaArticolo(this.stripNonDigitsChars(quantity))
      this.isQuantityValid = true
    }
  }

  async submitArticle() {
    const articolo = {
      codice: parseInt(this.props.views.codiceArticolo),
      scaffale: parseInt(this.props.views.scaffale),
      quantita: parseFloat(this.props.views.quantitaArticolo),
      settore: parseInt(this.props.views.corsia)
    }
    Log.log(`Inserimento articolo: <Articoli> ${JSON.stringify(articolo)}`)
    // TODO: implement something like DB.insertArticle(articolo)
    await DB.insertArticolo(articolo)
    Snackbar.show({
      title: "Articolo inserito",
      duration: Snackbar.LENGTH_SHORT
    })
    this.setNewArticle()
  }

  setNewArticle = () => {
    this.props.views.setCodiceArticolo("")
    if (this.props.views.tipoConta === "singola") {
      this.props.views.setQuantitaArticolo("1")
    } else {
      this.props.views.setQuantitaArticolo("")
    }
    if (this.ArticleCode !== null) {
      this.ArticleCode.focus()
    }
  }

  setNewScaffale = () => {
    this.props.views.setCodiceScaffale("")
    this.props.views.setScanType("scaffale")
  }

  isEmptyDescrizioneArticolo = () => {
    if (_.isEmpty(this.props.views.descrizioneArticolo)) {
      Alert.alert(
        "Attenzione",
        "Codice articolo non presente",
        [{ text: "OK" }],
        { cancelable: false }
      )
      this.props.views.setCodiceArticolo("")
      return true
    }
    return false
  }

  componentDidMount() {
    if (this.props.views.tipoConta === "singola") {
      this.props.views.setQuantitaArticolo("1")
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <TextInput
            selectTextOnFocus={true}
            style={styles.codiceArticolo}
            placeholder="Codice"
            value={
              this.props.views.codiceArticolo
                ? this.props.views.codiceArticolo.toString()
                : ""
            }
            ref={el => (this.ArticleCode = el)}
            onChangeText={text =>
              this.props.views.setCodiceArticolo(this.stripNonDigitsChars(text))
            }
            onBlur={() => this.handleArticleCode()}
            keyboardType="numeric"
            placeholderTextColor={theme.disabledTextColor}
            selectionColor={theme.secondaryColor}
            underlineColorAndroid={theme.secondaryColor}
          />
          <TextInput
            style={styles.descrizioneArticolo}
            placeholder="Descrizione"
            editable={false}
            value={
              this.props.views.descrizioneArticolo &&
              !_.isEmpty(this.props.views.descrizioneArticolo)
                ? this.props.views.descrizioneArticolo
                : ""
            }
            placeholderTextColor={theme.disabledTextColor}
            underlineColorAndroid={theme.disabledTextColor}
          />
          <View style={styles.quantitaWrapper}>
            <Text style={styles.quantitaText}>Quantità</Text>
            <TextInput
              onFocus={() => this.disableCamera()}
              onBlur={() => this.enableCamera()}
              selectTextOnFocus={true}
              style={[
                styles.quantitaInput,
                this.props.views.tipoConta === "singola" &&
                  styles.quantitaInputDisabled
              ]}
              ref={el => (this.QuantitaInput = el)}
              value={
                this.props.views.quantitaArticolo
                  ? this.props.views.quantitaArticolo.toString()
                  : ""
              }
              onChangeText={text => this.handleQuantity(text)}
              keyboardType="numeric"
              editable={this.props.views.tipoConta === "singola" ? false : true}
              placeholderTextColor={
                this.props.views.tipoConta === "singola"
                  ? theme.disabledTextColor
                  : theme.secondayTextColor
              }
              selectionColor={
                this.props.views.tipoConta === "singola"
                  ? theme.disabledTextColor
                  : theme.secondaryColor
              }
              underlineColorAndroid={
                this.props.views.tipoConta === "singola"
                  ? theme.disabledTextColor
                  : theme.secondaryColor
              }
            />
            {true ? (
              <Text style={styles.alreadyReadQuantity}>
                Quantità già letta:{" "}
                {this.props.views.alreadyReadQuantity === 0
                  ? "0"
                  : this.props.views.alreadyReadQuantity.toFixed(2)}
              </Text>
            ) : null}
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.newScaffaleButton}
            onPress={() => this.setNewScaffale()}
          >
            <Text style={styles.newScaffaleText}>NUOVO SCAFFALE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.newArticleButton}
            disabled={
              !this.props.views.descrizioneArticolo ||
              !this.props.views.descrizioneArticolo.length
            }
            onPress={() => this.submitArticle()}
          >
            <Text style={styles.newArticleText}>SALVA</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.grey3,
    marginLeft: theme.spacing3,
    marginRight: theme.spacing3
  },
  formContainer: {},
  codiceArticolo: {
    fontSize: theme.fontSize3,
    marginBottom: -10
  },
  descrizioneArticolo: {
    fontSize: theme.fontSize3,
    marginBottom: -10
  },
  quantitaWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  quantitaInput: {
    flex: 3,
    marginRight: theme.spacing4,
    color: theme.primaryTextColor
  },
  quantitaInputDisabled: {
    color: theme.disabledTextColor
  },
  quantitaText: {
    flex: 2,
    color: theme.primaryTextColor,
    marginLeft: theme.spacing2
  },
  alreadyReadQuantity: {
    flex: 4
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: theme.spacing4
  },
  newScaffaleButton: {
    paddingTop: theme.spacing4,
    paddingBottom: theme.spacing4,
    marginBottom: theme.spacing3
  },
  newScaffaleText: {
    fontSize: theme.fontSize3,
    color: theme.secondaryColor,
    fontWeight: "800"
  },
  newArticleButton: {
    paddingTop: theme.spacing4,
    paddingBottom: theme.spacing4,
    marginBottom: theme.spacing3,
    backgroundColor: theme.secondaryColor,
    paddingLeft: theme.spacing5,
    paddingRight: theme.spacing5
  },
  newArticleText: {
    color: theme.whiteTextColor,
    fontWeight: "800",
    alignSelf: "center"
  }
})

AppRegistry.registerComponent("Inventari", () => Articoli)
