import React from "react"
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  InteractionManager
} from "react-native"
import * as theme from "theme"
import Icon from "react-native-vector-icons/MaterialIcons"
import { observer } from "mobx-react"
import { observable } from "mobx"

@observer
export default class HomeScreen extends React.Component {
  @observable loadingView
  constructor(props) {
    super(props)
    this.loadingView = true
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.loadingView = false
    })
  }

  static navigationOptions = ({ navigation }) => ({
    title: "Home",
    drawerIcon: ({ tintColor }) => {
      return <Icon name="home" size={24} color={tintColor} />
    },
    headerLeft: (
      <TouchableOpacity
        style={styles.backButton}
        onPress={() => navigation.navigate("DrawerOpen")}
      >
        <Icon style={styles.backIcon} name="menu" />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: theme.primaryColor
    },
    headerTitleStyle: {
      color: theme.white,
      width: "100%"
    }
  })

  render() {
    if (this.loadingView) {
      return null
    }
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={theme.primaryColorDark}
          barStyle="light-content"
        />
        <TouchableOpacity
          style={styles.button}
          title="Importa"
          onPress={() => this.props.navigation.navigate("Importa")}
        >
          <Text style={styles.buttonText}>IMPORTA</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          title="Esporta"
          onPress={() => this.props.navigation.navigate("Esporta")}
        >
          <Text style={styles.buttonText}>ESPORTA</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          title="Scansione"
          onPress={() => this.props.navigation.navigate("SceltaDrawer")}
        >
          <Text style={styles.buttonText}>SCANSIONE</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    flex: 1,
    backgroundColor: theme.grey3
  },
  button: {
    backgroundColor: theme.secondaryColor,
    padding: theme.spacing4,
    margin: theme.spacing3
  },
  buttonText: {
    fontWeight: "bold",
    color: theme.whiteTextColor,
    fontSize: theme.fontSize3,
    textAlign: "center"
  },
  backIcon: {
    fontSize: theme.fontSizeIcon,
    color: theme.whiteTextColor,
    paddingLeft: theme.spacing4
  }
})

AppRegistry.registerComponent("Inventari", () => HomeScreen)
