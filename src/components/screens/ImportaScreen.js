import React from "react"
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  InteractionManager,
  Alert
} from "react-native"
import { observer, inject } from "mobx-react"
import { observable } from "mobx"
import DownloadProgress from "views/downloadprogress"
import configs from "configs"
import RNFetchBlob from "react-native-fetch-blob"
import DB from "db"
import * as theme from "theme"
import Snackbar from "react-native-snackbar"
import Log from "log"
import DefaultButton from "views/defaultbutton"

@inject("fs")
@observer
export default class ImportaScreen extends React.Component {
  @observable isActive
  @observable downloadCompleted
  @observable importActive
  @observable importComplete
  @observable showData

  @observable ArticoliDownloadActive
  @observable ArticoliDownloadCompleted
  @observable ArticoliDownloaded
  @observable ArticoliImportActive
  @observable ArticoliImportCompleted

  @observable SettoriDownloadActive
  @observable SettoriDownloadCompleted
  @observable SettoriDownloaded
  @observable SettoriImportActive
  @observable SettoriImportCompleted

  @observable SettoriMancantiDownloadActive
  @observable SettoriMancantiDownloadCompleted
  @observable SettoriMancantiDownloaded
  @observable SettoriMancantiImportActive
  @observable SettoriMancantiImportCompleted

  @observable logExample
  @observable warning
  @observable type
  @observable loadingView
  constructor(props) {
    super(props)
    this.isActive = false
    this.downloadCompleted = false
    this.importActive = false
    this.importComplete = false
    this.showData = true
    this.warning = null
    this.type = null
    this.loadingView = true
    this.ArticoliDownloadActive = false
    this.ArticoliDownloadCompleted = false
    this.ArticoliDownloaded = false
    this.ArticoliImportActive = false
    this.ArticoliImportCompleted = false

    this.SettoriDownloadActive = false
    this.SettoriDownloadCompleted = false
    this.SettoriDownloaded = false
    this.SettoriImportActive = false
    this.SettoriImportCompleted = false

    this.SettoriMancantiDownloadActive = false
    this.SettoriMancantiDownloadCompleted = false
    this.SettoriMancantiDownloaded = false
    this.SettoriMancantiImportActive = false
    this.SettoriMancantiImportCompleted = false
  }

  static navigationOptions = () => ({
    title: "Importa dati",
    headerStyle: {
      backgroundColor: theme.primaryColor
    },
    headerTitleStyle: {
      color: theme.white,
      width: "100%"
    },
    headerTintColor: theme.white
  })

  async componentDidMount() {
    // Check if default download path exists or create one
    InteractionManager.runAfterInteractions(() => {
      this.loadingView = false
    })
    try {
      await RNFetchBlob.fs.mkdir(configs.downloadPath)
    } catch (error) {
      Log.log("Cartella inventari già creata. Non verrà sovrascritta")
    }
  }

  async setNewInventario() {
    this.isActive = true
    await this.deleteFiles()
    DB.emptyDB()
    Snackbar.show({
      title: "Pronto per un nuovo inventario",
      duration: Snackbar.LENGTH_SHORT
    })
    this.isActive = false
  }

  /**
   * Delete the Inventari folder with its content
   *  and create a new one
   */
  async deleteFiles() {
    try {
      await RNFetchBlob.fs.unlink(configs.downloadPath)
      await RNFetchBlob.fs.mkdir(configs.downloadPath)
    } catch (err) {
      Log.error(err)
    }
  }

  async checkExistingFile(type) {
    // Check if file already exists
    if (!type) {
      return false
    }
    let exists
    switch (type) {
      case "articoli":
        exists = await RNFetchBlob.fs.exists(
          configs.downloadPath + configs.articoliFileName
        )
        break
      case "settori":
        exists = await RNFetchBlob.fs.exists(
          configs.downloadPath + configs.settoriFileName
        )
        break
      case "settorimancanti":
        exists = await RNFetchBlob.fs.exists(
          configs.downloadPath + configs.settoriMancantiFileName
        )
        break
      default:
        exists = false
        break
    }
    return exists
  }

  async downloadFile(type) {
    Log.log("Inizio download file " + type)
    if (!type) return false
    let fileName, downloadLink
    switch (type) {
      case "articoli":
        this.ArticoliDownloadActive = true
        fileName = configs.articoliFileName
        downloadLink = configs.articoliDownloadLink
        break
      case "settori":
        this.SettoriDownloadActive = true
        fileName = configs.settoriFileName
        downloadLink = configs.settoriDownloadLink
        break
      case "settorimancanti":
        this.SettoriMancantiDownloadActive = true
        fileName = configs.settoriMancantiFileName
        downloadLink = configs.settoriMancantiDownloadLink
        break
    }
    try {
      await RNFetchBlob.config({
        addAndroidDownloads: {
          notification: true,
          useDownloadManager: true, // <-- this is the only thing required
          // Optional, but recommended since android DownloadManager will fail when
          // the url does not contains a file extension, by default the mime type will be text/plain
          mime: "text/plain",
          description: `Scaricamento ${fileName} `,
          mediaScannable: true,
          path: configs.downloadPath + fileName
        }
      }).fetch("GET", downloadLink)
    } catch (error) {
      Log.error(error)
    }
    switch (type) {
      case "articoli":
        this.ArticoliDownloadActive = false
        this.ArticoliDownloadCompleted = true
        break
      case "settori":
        this.SettoriDownloadActive = false
        this.SettoriDownloadCompleted = true
        break
      case "settorimancanti":
        this.SettoriMancantiDownloadActive = false
        this.SettoriMancantiDownloadCompleted = true
        break
    }
    this.downloadCompleted = true
  }

  async startImporting() {
    this.showData = false
    this.type = "articoli"
    await this.downloadAndImportFile(this.type)
    this.type = "settori"
    await this.downloadAndImportFile(this.type)
    this.type = "settorimancanti"
    await this.downloadAndImportFile(this.type)
    this.showData = true
  }

  async deleteFile(type) {
    switch (type) {
      case "articoli":
        await RNFetchBlob.fs.unlink(
          configs.downloadPath + configs.articoliFileName
        )
        break
      case "settori":
        await RNFetchBlob.fs.unlink(
          configs.downloadPath + configs.settoriFileName
        )
        break
      case "settorimancanti":
        await RNFetchBlob.fs.unlink(
          configs.downloadPath + configs.settoriMancantiFileName
        )
        break
      default:
        break
    }
  }

  async downloadAndImportFile(type) {
    this.isActive = true
    if (!type) return false
    await this.deleteFile(type)
    await this.downloadFile(type)
    this.importActive = true
    DB.emptyDB(type)
    switch (type) {
      case "articoli":
        this.ArticoliImportActive = true
        break
      case "settori":
        this.SettoriImportActive = true
        break
      case "settorimancanti":
        this.SettoriMancantiImportActive = true
        break
    }
    await DB.importAnagrafica(type)
    switch (type) {
      case "articoli":
        this.ArticoliImportActive = false
        this.ArticoliImportCompleted = true
        break
      case "settori":
        this.SettoriImportActive = false
        this.SettoriImportCompleted = true
        break
      case "settorimancanti":
        this.SettoriMancantiImportActive = false
        this.SettoriMancantiImportCompleted = true
        break
    }
    this.isActive = false
    this.importComplete = true
  }

  render() {
    if (this.loadingView) {
      return null
    }
    return (
      <ScrollView style={ss.container}>
        <DefaultButton
          title="INIZIALIZZA INVENTARIO"
          disabled={this.isActive}
          onPress={() => {
            const setNewInventario = this.setNewInventario.bind(this)
            Alert.alert(
              "Conferma inizializzazione",
              `Sei sicuro di volere inizializzare l'inventario? Tutti i files e i dati nel db verrano cancellati`,
              [
                {
                  text: "Annulla",
                  onPress: () => Log.log("annullato")
                },
                {
                  text: "Conferma",
                  onPress: () => setNewInventario()
                }
              ]
            )
          }}
        />
        <DefaultButton
          title="SCARICA E IMPORTA ANAGRAFICA"
          disabled={this.isActive}
          onPress={() => this.startImporting()}
        />
        {this.warning ? (
          <Text style={ss.warningText}> {this.warning} </Text>
        ) : null}
        <View style={ss.typeContainer}>
          <DownloadProgress
            style={ss.downloadProgress}
            type="Articoli"
            download={true}
            active={this.ArticoliDownloadActive}
            completed={this.ArticoliDownloadCompleted}
            alreadyDownloaded={this.ArticoliDownloaded}
          />
          <DownloadProgress
            type="Articoli"
            download={false}
            active={this.ArticoliImportActive}
            completed={this.ArticoliImportCompleted}
          />
        </View>
        <View style={ss.typeContainer}>
          <DownloadProgress
            style={ss.downloadProgress}
            type="Scaffali"
            download={true}
            active={this.SettoriDownloadActive}
            completed={this.SettoriDownloadCompleted}
            alreadyDownloaded={this.SettoriDownloaded}
          />
          <DownloadProgress
            type="Scaffali"
            download={false}
            active={this.SettoriImportActive}
            completed={this.SettoriImportCompleted}
          />
        </View>
        <View style={ss.typeContainer}>
          <DownloadProgress
            style={ss.downloadProgress}
            type="Scaffali Mancanti"
            download={true}
            active={this.SettoriMancantiDownloadActive}
            completed={this.SettoriMancantiDownloadCompleted}
            alreadyDownloaded={this.SettoriMancantiDownloaded}
          />
          <DownloadProgress
            type="Scaffali Mancanti"
            download={false}
            active={this.SettoriMancantiImportActive}
            completed={this.SettoriMancantiImportCompleted}
          />
        </View>
        {this.showData ? (
          <View style={ss.dataContainer}>
            <Text style={ss.dataTitle}>Articoli</Text>
            <Text style={ss.dataItem}>
              Articoli importati: {DB.articoliAnagrafica.length}
            </Text>
            <FlatList
              data={DB.articoliAnagrafica.slice(0, 4)}
              renderItem={({ item }) => (
                <Text style={ss.dataItem}>
                  Codice: {item.codice}
                  {"\n"}Descrizione: {item.descrizione}
                </Text>
              )}
              keyExtractor={(item, index) => index}
            />
            <Text style={ss.dataTitle}>Settori</Text>
            <FlatList
              data={DB.settoriAnagrafica}
              renderItem={({ item }) => (
                <Text style={ss.dataItem}>
                  Corsia: {item.settore}
                  {"\t\t\t"}Ultimo scaffale: {item.ultimoScaffale}
                </Text>
              )}
              keyExtractor={(item, index) => index}
            />
            <Text style={ss.dataTitle}>Settori mancanti</Text>
            <FlatList
              data={DB.settoriMancanti}
              renderItem={({ item }) => (
                <Text style={ss.dataItem}>
                  Corsia: {item.settore}
                  {"\t\t\t"}Scaffale: {item.scaffale}
                </Text>
              )}
              keyExtractor={(item, index) => index}
            />
          </View>
        ) : null}
      </ScrollView>
    )
  }
}

const ss = StyleSheet.create({
  container: {
    padding: theme.spacing3
  },
  typeContainer: {
    margin: theme.spacing3
  },
  dataContainer: {
    backgroundColor: theme.white,
    margin: theme.spacing3,
    paddingBottom: theme.spacing4,
    marginBottom: theme.spacing5
  },
  dataTitle: {
    fontWeight: "800",
    color: theme.primaryTextColor,
    padding: theme.spacing4,
    paddingTop: theme.spacing5,
    paddingBottom: theme.spacing3,
    fontSize: theme.fontSize2
  },
  dataItem: {
    fontWeight: "100",
    fontFamily: "sans-serif-light",
    color: theme.primaryTextColor,
    paddingLeft: theme.spacing4,
    paddingRight: theme.spacing4,
    paddingTop: theme.spacing3
  }
})

AppRegistry.registerComponent("Inventari", () => ImportaScreen)
