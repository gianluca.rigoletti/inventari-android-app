import React from "react"
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Alert,
  ProgressBarAndroid,
  FlatList,
  InteractionManager,
  ScrollView,
  TextInput
} from "react-native"
import { observer, inject } from "mobx-react"
import { observable, computed } from "mobx"
import DB from "db"
import configs from "configs"
import RNFetchBlob from "react-native-fetch-blob"
import * as theme from "theme"
import Icon from "react-native-vector-icons/MaterialIcons"
import IMEI from "react-native-imei"
import Log from "log"
import DefaultButton from "views/defaultbutton"
import Fuse from "fuse.js"

@inject("views")
@observer
export default class EsportaScreen extends React.Component {
  @observable isExporting
  @observable exportComplete
  @observable uploadComplete
  @observable isUploading
  @observable isFlushing
  @observable loadingView
  @observable emptyExport
  @observable articoli
  filePath
  fileName
  constructor(props) {
    super(props)
    this.isExporting = false
    this.exportComplete = false
    this.isUploading = false
    this.uploadComplete = false
    this.loadingView = true
    this.isFlushing = false
    this.articoli = DB.getArticoli()
    this.fuse = new Fuse(this.articoli, {
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ["codice", "settore", "scaffale", "quantita", "esportato"]
    })
    this.filePath = ""
    this.fileName = ""
    this.emptyExport = false
  }

  @computed
  get activeProcess() {
    return this.isExporting || this.isUploading || this.isFlushing
  }

  static navigationOptions = () => ({
    title: "Esporta database",
    headerStyle: {
      backgroundColor: theme.primaryColor
    },
    headerTitleStyle: {
      color: theme.white,
      width: "100%"
    },
    headerTintColor: theme.white
  })

  uploadFile = async fileName => {
    let result = false
    const filePath = configs.downloadPath + fileName
    Log.log(`Filename in caricamento: ${this.filePath} ${this.fileName}`)
    return RNFetchBlob.fetch(
      "POST",
      configs.uploadPath,
      {
        "Content-Type": "multipart/form-data"
      },
      [
        {
          name: "sampleFile",
          filename: fileName,
          data: RNFetchBlob.wrap(filePath)
        }
      ]
    )
      .then(resp => {
        if (resp.respInfo.status === 200) {
          result = true
        }
        return result
      })
      .catch(err => {
        Log.error(err)
        Alert.alert("Si è verificato un errore nel caricare il file.")
        return result
      })
  }

  async handleExport(force) {
    this.isExporting = true
    const articoli = DB.getArticoli()
    Log.log(`Inizio esportazione articoli`)
    const formattedDate = force
      ? new Date()
          .toISOString()
          .slice(0, 10)
          .replace(/-/g, "")
      : new Date().getTime()
    const fileName = force
      ? `${IMEI.getImei()}_${formattedDate}_totale_export.csv`
      : `${IMEI.getImei()}_${formattedDate}_export.csv`
    this.fileName = fileName
    const filePath = configs.downloadPath + fileName
    Log.log(`Percorso file: ${filePath}`)
    const result = await DB.saveArticoliToCsv(filePath, force)
    this.exportComplete = true
    this.isExporting = false
    if (!result) {
      this.emptyExport = true
      return false
    }
    this.filePath = filePath
    Log.log(`Esportati ${articoli.length} articoli in ${filePath}`)
    return true
  }

  uploadFiles = async () => {
    const files = await RNFetchBlob.fs.lstat(configs.downloadPath)
    this.isUploading = true
    this.isExporting = false
    const results = await Promise.all(
      files
        .filter(el => {
          return el.type === "file"
        })
        .filter(el => {
          return /\d+_\d+_\w+\.csv$/.test(el.filename)
        })
        .map(async el => {
          const isUploaded = await this.uploadFile(el.filename)
          if (/\d+_\d+_[a-zA-Z]+\.csv$/.test(el.filename) && isUploaded) {
            await RNFetchBlob.fs.mv(
              configs.downloadPath + el.filename,
              configs.downloadPath + el.filename + ".exported.csv"
            )
          }
          return isUploaded
        })
    )
    console.log(results)
    this.isUploading = false
    this.uploadComplete = true
  }

  async exportAndUpload(force = false) {
    const result = await this.handleExport(force)
    if (result) {
      this.uploadFiles()
    }
  }

  searchArticoli = text => {
    const results = this.fuse.search(text)
    this.articoli = results.length ? results : DB.getArticoli()
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.loadingView = false
    })
  }

  render() {
    if (this.loadingView) {
      return null
    }
    return (
      <ScrollView style={ss.container}>
        <DefaultButton
          disabled={this.activeProcess}
          alert={false}
          onPress={this.exportAndUpload.bind(this)}
          title="CARICA ED ESPORTA"
        />
        <DefaultButton
          disabled={this.activeProcess}
          alert={true}
          title="CARICA ED ESPORTA TUTTO"
          onPress={() => {
            const exportAndUpload = this.exportAndUpload.bind(this)
            Alert.alert(
              "Conferma esportazione",
              "Sei sicuro di volere esportare tutto?",
              [
                {
                  text: "Annulla",
                  onPress: () => Log.log("Annullata conferma esportazione")
                },
                {
                  text: "Conferma",
                  onPress: () => exportAndUpload(true)
                }
              ]
            )
          }}
        />
        {this.emptyExport ? (
          <View style={ss.exportContainer}>
            <Text style={ss.exportComplete}>
              Record già scritti. Nessun file esportato
            </Text>
            <Icon name="done" style={ss.icon} />
          </View>
        ) : this.exportComplete ? (
          <View style={ss.exportContainer}>
            <Text style={ss.exportComplete}>Esportazione completata</Text>
            <Icon name="done" style={ss.icon} />
            <Text style={ss.exportComplete}>File esportato come:</Text>
            <Text style={ss.fileName}>{this.fileName}</Text>
          </View>
        ) : null}
        {this.isExporting ? (
          <View style={ss.exportProgressContainer}>
            <Text style={ss.exportInProgress}>Sto esportando</Text>
            <ProgressBarAndroid
              styleAttr="Horizontal"
              color={theme.secondaryColor}
            />
          </View>
        ) : null}
        {this.isUploading ? (
          <View style={ss.uploadProgressContainer}>
            <Text style={ss.uploadInProgress}>Sto caricando il file</Text>
            <ProgressBarAndroid
              styleAttr="Horizontal"
              color={theme.secondaryColor}
            />
          </View>
        ) : null}
        {this.uploadComplete ? (
          <View style={ss.uploadContainer}>
            <Text style={ss.uploadComplete}>Upload completato</Text>
            <Icon name="done" style={ss.icon} />
          </View>
        ) : null}
        {this.articoli.length ? (
          <View style={ss.dataContainer}>
            <View style={ss.searchContainer}>
              <TextInput
                selectTextOnFocus={true}
                underlineColorAndroid={theme.secondaryColorLight}
                selectionColor={theme.secondaryColorLight}
                style={ss.searchInput}
                placeholder="Ricerca..."
                onChangeText={text => this.searchArticoli(text)}
              />
            </View>
            <FlatList
              data={this.articoli}
              renderItem={({ item }) => (
                <Text style={ss.dataItem}>
                  Codice: {item.codice}
                  {"\n"}Settore: {item.settore}
                  {"\t\t"}Scaffale: {item.scaffale}
                  {"\t\t"}Quantità: {item.quantita}
                  {"\n"}Esportato: {item.esportato ? "sì" : "no"}
                </Text>
              )}
              keyExtractor={item => item.id}
            />
          </View>
        ) : null}
      </ScrollView>
    )
  }
}

const ss = StyleSheet.create({
  container: {
    padding: theme.spacing3
  },
  icon: {
    fontSize: theme.fontSizeIcon,
    color: theme.successColor,
    alignSelf: "center"
  },
  exportContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    backgroundColor: theme.white,
    margin: theme.spacing3,
    padding: theme.spacing4,
    minHeight: theme.spacing5 * 2
  },
  exportProgressContainer: {
    backgroundColor: theme.white,
    margin: theme.spacing3,
    padding: theme.spacing4,
    minHeight: theme.spacing5 * 2
  },
  exportInProgress: {},
  exportComplete: {
    color: theme.primaryTextColor
  },
  uploadContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: theme.white,
    margin: theme.spacing3,
    padding: theme.spacing4,
    minHeight: theme.spacing5 * 2
  },
  uploadProgressContainer: {
    backgroundColor: theme.white,
    margin: theme.spacing3,
    padding: theme.spacing4,
    minHeight: theme.spacing5 * 2
  },
  uploadComplete: {},
  uploadInProgress: {},
  dataContainer: {
    backgroundColor: theme.white,
    margin: theme.spacing3,
    paddingTop: theme.spacing4,
    paddingBottom: theme.spacing4,
    marginBottom: theme.spacing5
  },
  searchContainer: {
    paddingLeft: theme.spacing4,
    paddingRight: theme.spacing4
  },
  fileName: {
    fontFamily: "monospace"
  },
  dataItem: {
    fontWeight: "100",
    fontFamily: "sans-serif-light",
    color: theme.primaryTextColor,
    paddingLeft: theme.spacing4,
    paddingRight: theme.spacing4,
    paddingTop: theme.spacing3
  }
})

AppRegistry.registerComponent("Inventari", () => EsportaScreen)
