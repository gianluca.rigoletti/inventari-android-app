import React from "react"
import { AppRegistry, View, TouchableOpacity, StatusBar } from "react-native"
import { observer, inject } from "mobx-react"
import Icon from "react-native-vector-icons/Ionicons"
import {
  spacing3,
  spacing4,
  fontSizeIcon,
  secondaryColor,
  grey3,
  whiteTextColor,
  primaryColor,
  primaryColorDark
} from "theme"
import DefaultButton from "views/defaultbutton"

@inject("views", "navigators")
@observer
export default class SceltaScreen extends React.Component {
  constructor(props) {
    super(props)
  }

  static navigationOptions = ({ navigation }) => ({
    title: "Scelta scansione",
    drawerIcon: ({ tintColor }) => {
      return <Icon name="md-camera" size={24} color={tintColor} />
    },
    headerLeft: (
      <TouchableOpacity
        style={styles.backButton}
        onPress={() => navigation.goBack(null)}
      >
        <Icon style={styles.backIcon} name="md-arrow-back" />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: primaryColor
    },
    headerTitleStyle: {
      color: whiteTextColor,
      width: "100%"
    },
    headerTintColor: whiteTextColor
  })

  setScan = type => {
    switch (type) {
      case "singola":
        this.props.views.setTipoConta("singola")
        break
      case "multipla":
        this.props.views.setTipoConta("multipla")
        break
      default:
        this.props.views.setTipoConta("singola")
        break
    }
    this.props.views.setScanType("scaffale")
    this.props.views.setCodiceScaffale("")
    this.props.navigation.navigate("Scan")
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={primaryColorDark}
          barStyle="light-content"
        />
        <DefaultButton
          title="CONTA SINGOLA"
          onPress={() => this.setScan("singola")}
        />
        <DefaultButton
          title="CONTA MULTIPLA"
          onPress={() => this.setScan("multipla")}
        />
      </View>
    )
  }
}

const styles = {
  container: {
    flexDirection: "column",
    flex: 1,
    backgroundColor: grey3
  },
  button: {
    backgroundColor: secondaryColor,
    padding: spacing4,
    margin: spacing3
  },
  buttonText: {
    fontWeight: "bold",
    color: whiteTextColor,
    alignSelf: "center"
  },
  backButton: {
    padding: spacing4
  },
  backIcon: {
    fontSize: fontSizeIcon,
    color: whiteTextColor
  }
}

AppRegistry.registerComponent("Inventari", () => SceltaScreen)
