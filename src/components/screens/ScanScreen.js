import React from "react"
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  InteractionManager,
  ActivityIndicator
} from "react-native"
import { observer, inject } from "mobx-react"
import { observable } from "mobx"
import Articoli from "views/articoli"
import Scaffale from "views/scaffale"
import ScanCamera from "views/scancamera"
import * as theme from "theme"
import Icon from "react-native-vector-icons/Ionicons"

@inject("views")
@observer
export default class ScanScreen extends React.Component {
  @observable loadingView
  constructor(props) {
    super(props)
    this.loadingView = true
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.loadingView = false
    })
  }

  static navigationOptions = () => ({
    header: null,
    headerStyle: {
      backgroundColor: theme.primaryColor
    },
    headerTitleStyle: {
      color: theme.white,
      width: "100%"
    },
    headerTintColor: theme.white
  })

  render() {
    if (this.loadingView) {
      return (
        <View style={styles.loadingView}>
          <ActivityIndicator size="large" color={theme.primaryColor} />
        </View>
      )
    }
    let renderingComponent
    if (this.props.views.scanType === "singola/multipla") {
      renderingComponent = <Articoli style={styles.renderingComponent} />
    } else {
      renderingComponent = <Scaffale style={styles.renderingComponent} />
    }
    return (
      <View style={styles.container}>
        <ScanCamera style={styles.cameraContainer}>
          <View style={styles.headerContainer}>
            <TouchableOpacity
              style={styles.headerButtonContainer}
              onPress={() => this.props.navigation.goBack(null)}
            >
              <Icon style={styles.iconBack} name="md-arrow-back" />
            </TouchableOpacity>
            <Text style={styles.headerText}>{this.props.views.scanType}</Text>
            <Text style={styles.codiceScaffaleText}>
              {this.props.views.codiceScaffale}
            </Text>
          </View>
        </ScanCamera>
        {renderingComponent}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loadingView: {
    alignItems: "center",
    justifyContent: "center",
    flexGrow: 1
  },
  container: {
    flexDirection: "column",
    flexGrow: 1,
    backgroundColor: theme.grey3
  },
  headerContainer: {
    height: 64,
    position: "absolute",
    zIndex: 2,
    width: "100%",
    backgroundColor: theme.transparent,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  headerButtonContainer: {
    flex: 1,
    alignSelf: "center",
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    alignItems: "center"
  },
  iconBack: {
    color: theme.whiteTextColor,
    fontSize: theme.fontSizeIcon,
    textShadowColor: theme.primaryTextColor,
    textShadowOffset: {
      width: 1,
      height: 1
    },
    textShadowRadius: 3
  },
  headerText: {
    flex: 5,
    fontWeight: "bold",
    color: theme.whiteTextColor,
    textShadowColor: theme.primaryTextColor,
    fontSize: theme.fontSize3,
    textShadowOffset: {
      width: 1,
      height: 1
    },
    textShadowRadius: 2
  },
  codiceScaffaleText: {
    fontWeight: "bold",
    fontSize: theme.fontSize3,
    color: theme.whiteTextColor,
    paddingRight: theme.spacing4,
    textShadowOffset: {
      width: 1,
      height: 1
    }
  },
  cameraContainer: {},
  renderingComponent: {
    flex: 1
  }
})

AppRegistry.registerComponent("Inventari", () => ScanScreen)
