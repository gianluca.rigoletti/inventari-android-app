import RNFetchBlob from "react-native-fetch-blob"
import configs from "configs"

export const readCsv = async type => {
  let filePath
  switch (type) {
    case "articoli":
      filePath = configs.downloadPath + configs.articoliFileName
      break
    case "settori":
      filePath = configs.downloadPath + configs.settoriFileName
      break
    case "settorimancanti":
      filePath = configs.downloadPath + configs.settoriMancantiFileName
      break
    default:
      filePath = null
      break
  }
  let data = null
  try {
    data = await RNFetchBlob.fs.readFile(filePath, "utf8")
  } catch (error) {
    console.log(`Errore file lettura`)
    console.log(error)
  }
  return data
}
