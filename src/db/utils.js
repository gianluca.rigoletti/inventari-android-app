export const getId = (collection) => {
    const maxId = _.max(_.map(collection, _.property('id')));
    return (maxId !== undefined) ? maxId : 1;
};