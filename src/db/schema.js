export const ArticoliAnagraficaSchema = {
  name: "ArticoliAnagrafica",
  properties: {
    codice: "int",
    descrizione: "string"
  }
}

export const ArticoliSchema = {
  name: "Articoli",
  primaryKey: "id",
  properties: {
    id: "int",
    codice: "int",
    settore: "int",
    scaffale: "int",
    quantita: "float",
    creato: "date",
    esportato: {
      type: "bool",
      default: false
    }
  }
}

export const SettoriAnagraficaSchema = {
  name: "SettoriAnagrafica",
  properties: {
    settore: "int",
    ultimoScaffale: "int"
  }
}

export const SettoriMancantiSchema = {
  name: "SettoriMancanti",
  properties: {
    settore: "int",
    scaffale: "int"
  }
}

export const SettoriSchema = {
  name: "Settori",
  properties: {
    corsia: "int",
    scaffale: "int",
    giaLetto: "bool"
  }
}
