import realm from "./realm"
import pp from "papaparse"
import { readCsv } from "./importer"
import stores from "stores"
import configs from "configs"
import RNFetchBlob from "react-native-fetch-blob"
import _ from "lodash"
import Log from "log"

export default class DB {
  articoli
  articoliAnagrafica
  settori
  settoriAnagrafica
  settoriMancanti
  enableLog

  constructor(log = false) {
    this.articoli = realm.objects("Articoli")
    this.articoliAnagrafica = realm.objects("ArticoliAnagrafica")
    this.settori = realm.objects("Settori")
    this.settoriAnagrafica = realm.objects("SettoriAnagrafica")
    this.settoriMancanti = realm.objects("SettoriMancanti")
    this.enableLog = log
  }

  get articoli() {
    return realm.objects("Articoli")
  }

  getArticoli(number = 0) {
    if (number !== 0) {
      return this.articoli.slice(0, number)
    }
    return this.articoli
  }

  getArticoloById(id) {
    return realm.objectForPrimaryKey("Articoli", id)
  }

  getId = collection => {
    const ids = [...collection.map(item => item.id), 1]
    let maxId = Math.max(...ids) + 1
    return maxId
  }

  getDescrizioneArticolo(cod) {
    const codice = parseInt(cod)
    Log.log(`getDescrizioneArticolo codice: ${codice}`)
    let articolo = null
    if (!isNaN(codice)) {
      articolo = this.articoliAnagrafica.filtered(`codice = "${cod}"`)
      if (articolo && articolo.length) {
        return articolo[0].descrizione
      }
    }
    return articolo
  }

  getExistingArticoli(cod, deep = false, settore = null, scaffale = null) {
    const codice = cod ? cod : ""
    let articoli = []
    try {
      if (deep) {
        articoli = this.articoli.filtered(
          `codice = ${codice} AND settore = ${settore} AND scaffale = ${scaffale}`
        )
      } else {
        articoli = this.articoli.filtered(`codice = ${codice}`)
      }
    } catch (err) {
      Log.error(`Errore filtro existing articoli: ${err}`)
    }
    Log.log(`Articoli filtrati: ${JSON.stringify(articoli)}`)
    return articoli
  }

  async insertArticolo(object) {
    try {
      realm.write(() => {
        const articolo = {
          ...object,
          id: this.getId(this.articoli),
          creato: new Date()
        }
        Log.log(`Inserimento nuovo articolo: ${JSON.stringify(articolo)}`)
        realm.create("Articoli", articolo)
      })
    } catch (error) {
      Log.error(error)
    }
  }

  insertSettore(object) {
    try {
      realm.write(() => {
        realm.create("Settori", { ...object, giaLetto: true })
      })
    } catch (error) {
      Log.error(error)
    }
  }

  isSettoreGiaLetto(corsia, scaffale) {
    const settore = this.settori.filtered(
      `corsia = ${corsia} AND scaffale = ${scaffale}`
    )
    if (settore && settore.length && settore[0].giaLetto) {
      return true
    }
    return false
  }

  isMissingOrInvalidSettore(corsia, scaffale) {
    const settoreMancante = this.settoriMancanti.filtered(
      `settore = ${corsia} AND scaffale = ${scaffale}`
    )
    const isMissing = settoreMancante && settoreMancante.length
    const corsiaAnagrafica = this.settoriAnagrafica.filtered(
      `settore = ${corsia}`
    )
    const isInvalid =
      !corsiaAnagrafica.length || corsiaAnagrafica[0].ultimoScaffale < scaffale
    Log.log(
      `L'ultimo scaffale per la corsia scelta è ${JSON.stringify(
        corsiaAnagrafica
      )}`
    )
    if (isMissing || isInvalid) {
      return true
    }
    return false
  }

  flushSettoriAndArticoli() {
    realm.write(() => {
      realm.delete(this.settori)
      realm.delete(this.articoli)
    })
  }

  async saveArticoliToCsv(defaultFileName, force = false) {
    const unexportedArticoli = !force
      ? this.articoli.filtered("esportato = $0", false).snapshot()
      : this.articoli
    if (!unexportedArticoli.length) return false
    try {
      await RNFetchBlob.fs.createFile(defaultFileName, "")
    } catch (error) {
      Log.log("File già creato in saveArticoliToCsv")
    }
    const stream = await RNFetchBlob.fs.writeStream(
      defaultFileName,
      "utf8",
      false
    )
    const chunks = _.chunk(unexportedArticoli, 100)
    chunks.map(chunk => {
      const csv = pp.unparse(chunk, {
        header: false,
        quotes: false,
        delimiter: ";"
      })
      stream.write(csv)
    })
    stream.close()
    if (!force) {
      realm.write(() => {
        unexportedArticoli.forEach(articolo => {
          realm.create("Articoli", { id: articolo.id, esportato: true }, true)
        })
      })
    }
    return true
  }

  async importAnagrafica(type) {
    const data = await readCsv(type)
    realm.write(() => {
      pp.parse(data, {
        step: row => {
          const line = row.data[0]
          try {
            switch (type) {
              case "articoli":
                realm.create("ArticoliAnagrafica", {
                  codice: parseInt(line[1]),
                  descrizione: line[2]
                })
                break
              case "settori":
                realm.create("SettoriAnagrafica", {
                  settore: parseInt(line[2]),
                  ultimoScaffale: parseInt(line[3])
                })
                break
              case "settorimancanti":
                realm.create("SettoriMancanti", {
                  settore: parseInt(line[1]),
                  scaffale: parseInt(line[3])
                })
                break
              default:
                break
            }
          } catch (error) {
            Log.error(`Errore in importazione in db ${type}:  ${error}`)
          }
        },
        complete: () => {
          Log.log("importAnagrafica Importazione completata")
          stores.fs.setImportProgress(1)
        }
      })
    })
  }

  emptyDB(type) {
    if (type !== undefined && type.length) {
      realm.write(() => {
        switch (type) {
          case "articoli":
            realm.delete(this.articoliAnagrafica)
            break
          case "settori":
            realm.delete(this.settoriAnagrafica)
            break
          case "settorimancanti":
            realm.delete(this.settoriMancanti)
            break
          default:
            break
        }
      })
    } else {
      realm.write(() => {
        realm.deleteAll()
      })
    }
  }

  forceEmptyDB() {
    realm.deleteFile({
      path: configs.dbPath
    })
  }
}
