import Realm from "realm"
import {
  ArticoliAnagraficaSchema,
  ArticoliSchema,
  SettoriSchema,
  SettoriAnagraficaSchema,
  SettoriMancantiSchema
} from "./schema"

export default new Realm({
  schema: [
    ArticoliAnagraficaSchema,
    ArticoliSchema,
    SettoriSchema,
    SettoriAnagraficaSchema,
    SettoriMancantiSchema
  ]
})
