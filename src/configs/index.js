import RNFetchBlob from "react-native-fetch-blob"
const dirs = RNFetchBlob.fs.dirs

export default {
  downloadPath: dirs.SDCardDir + "/Inventari/",
  articoliFileName: "articoli_reali.csv",
  settoriFileName: "dettaglio_settori.csv",
  settoriMancantiFileName: "dettaglio_settori_mancanti.csv",
  articoliDownloadLink:
    "http://192.168.0.100:8080/inventari/Inventari/Ges/dati/esporta/articoli_reali.csv",
  settoriDownloadLink:
    "http://192.168.0.100:8080/inventari/Inventari/Ges/dati/esporta/dettaglio_settori.csv",
  settoriMancantiDownloadLink:
    "http://192.168.0.100:8080/inventari/Inventari/Ges/dati/esporta/dettaglio_settori_mancanti.csv",
  dbPath: dirs.SDCardDir + "/Inventari/db.realm",
  logPath: dirs.SDCardDir + "/InventariLog/",
  uploadPath: "http://192.168.0.100:8080/inventari/Inventari/Ges/upload_csv.php"
}
