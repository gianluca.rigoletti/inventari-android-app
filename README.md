# Documentation

Per installare tutte le dipendenze: 

```bash
npm install
```
Per far partire l'ambiente:

```bash
react-native run-android
```

## Dipendenze

* [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)
* [react-navigation](https://reactnavigation.org/docs/intro/)
* [react-native-camera](https://github.com/lwansbrough/react-native-camera)

## Note
Per il DB conviene che la lettura sia sincrona e la scrittura asincrona
Per il DB, le api da mettere a disposizione devono essere:
    * `findOrCreateArticolo(codice)`
    * `insertArticolo(object)`
    * `getAllArticoli()`
    * `getArticoloById(id, schema)`
    * `findArticolo(codice)`
    * `insertSettore(object)`
    * `isSettoreGiàLetto(corsia, scaffale)`
    * `saveToCsv()`
    * `static importToCsv()`
    * `drop()'


## TODO
- [x] Controllo su settori esisteni da anagrafica settori
- [x] Controllo su articoli esistenti
- [x] Controllo su quantità e aggiunta soltanto
- [x] Inserimento settore particolare per quantità negativa
- [x] Performance fotocamera tra scaffali e articoli
- [x] UI
- [x] Implementazione suono quando viene scansionato un codice a barre
- [] Classe per log su file
- [x] Mostrare il numero di righe importate
- [x] Mettere virgola o punto per le quantità
- [x] Cancellare i file di anagrafica ogni volta che si scaricano
- [x] Se su settore 999999 mettere come quantità già lòetta la somma di tutte le quantità del relativo codice. 
Aggiungere un controllo per non inseriree una quantità negativa maggiore di quella già sparata
- [x] Cambiare lo schema del db mettendo un campo esportato negli articoli. In esportazione impostare il campo a true.
Quando clicco su esporta genera file univoco imei_timestamp_export.csv mettendo soltanto i record con esportato == false.
Una volta fatto tutti quei record vengono impostati a esportato == true. Togliere svuota db, esporta in csv e upload csv. 
Aggiungere un bottone carica ed esporta tutto da usare solo in caso di emergenza con nome imei_data_totale_export.csv
- [x] In importa aggiungere svuota db e chiamarlo inizializza inventario che toglie i file e svuota il db
- [x] mettere sempre un bottone di conferma per ogni azione compiuta
- [x] Doppio tap per uscire dall'applicazione

## Cose da discutere
- [x] Che dati visualizzare in esporta e se statici: non visualizzarli
- [x] Impostazione nome csv esportato. Per ora esporta come {IMEI}_{yyyymmdd}_export.csv
- [] Come gestire l'indirizzo di download ed upload dell'anagrafica. Per ora il download fa riferimento ad un file statico su gitlab, mentre l'upload è hardcoded nel codice giusto per test
- [] Gestione di un accesso protetto per esportare ed importare il db: non serve, mettere conferma
- [] Che dati di Log visualizzare
- [] Come gestire gli aggiornamenti: codepush, play store, server locale
- [] Eventuale logo

## Lato server
Aggiungere nella tabella articoli i campi:
- ID_RECORD_TERMINALE che corrisponde all'id del singolo record nel file csv esportato
- ingrandire il campo lettore per farci stare l'imei
- Controllare l'univocità della tupla (ID_INVENTARIO, LETTORE, ID_RECORD_TERMINALE)

Il programma lato server deve implementare una funzione `prepara_csv()`. La funzione deve esportare
in csv i file dell'inventario corrente che adesso vengono scaricati da gitlab e metterli su una cartella /csv

Inoltre, devono essere messe a disposizione le funzioni per caricare i files dai terminali: 
una funzione `upload()` che legge tutti i file nella cartella che sono stati caricati dai terminali e 
prova ad importarli. Se sono presenti record con quantità negative bisogna sottrarre la quantità
dai record precedenti a parità di imei, codice, id inventario.
Bisogna controllare l'univocità della tupla (scrivere qualcosa da qualche parte). E infine bisogna spostare i file caricati in una cartella `exported`.

Inoltre bisogna implementare una funzione `importa_tutto()` che controlla se sono presenti dei file con `_tutto.csv` nel nome.
Se sono presenti deve cancellare tutti i record dell'invnetario corrente con l'imei del terminale (campo `LETTORE`) e inserire
il contenuto del file nel db. Poi va spostarto nella cartella come gli altri files.
