import React from "react"
import { AppRegistry, BackHandler } from "react-native"
import RootNavigator from "navigators/RootNavigator"
import { observer, Provider } from "mobx-react"
import stores from "stores"
import { addNavigationHelpers, NavigationActions } from "react-navigation"
const Sound = require("react-native-sound")
import { setJSExceptionHandler } from "react-native-exception-handler"
import { setNativeExceptionHandler } from "react-native-exception-handler/index"
import Log from "log"

// registering the error handler (maybe u can do this in the index.android.js or index.ios.js)
setJSExceptionHandler((error, isFatal) => {
  Log.error(error)
}, true)

setNativeExceptionHandler(exceptionString => {
  Log.error(exceptionString)
})

@observer
export default class InventariApp extends React.Component {
  constructor(props) {
    super(props)
    this.lastBackButtonPress = null
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress)
    const beep = new Sound("beep.mp3", Sound.MAIN_BUNDLE, err => {
      if (err) {
        Log.error(err)
      }
      Log.log("Sound loaded")
    })
    stores.views.beep = beep
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress)
  }

  onBackPress = () => {
    const { dispatch } = stores.navigators
    if (this.lastBackButtonPress + 750 >= new Date().getTime()) {
      BackHandler.exitApp()
      return true
    }
    this.lastBackButtonPress = new Date().getTime()
    dispatch(NavigationActions.back())
    return true
  }

  render() {
    return (
      <Provider {...stores}>
        <RootNavigator
          navigation={addNavigationHelpers({
            dispatch: stores.navigators.dispatch,
            state: stores.navigators.navigationState
          })}
        />
      </Provider>
    )
  }
}

AppRegistry.registerComponent("Inventari", () => InventariApp)
